#!/usr/bin/python

#/***************************************************************************
#    Copyright 2008 Frederik Gladhorn <frederik.gladhorn@kdemail.net>
# ***************************************************************************/

#/***************************************************************************
# *                                                                         *
# *   This program is free software; you can redistribute it and/or modify  *
# *   it under the terms of the GNU General Public License as published by  *
# *   the Free Software Foundation; either version 2 of the License, or     *
# *   (at your option) any later version.                                   *
# *                                                                         *
# ***************************************************************************/

#import xml.dom.minidom

#xml - sax parsing
from xml.sax import make_parser
from xml.sax.handler import ContentHandler

#xml - sax - writing the meta files
from xml.sax.saxutils import XMLGenerator
from xml.sax import saxutils

#tar bz2 archives
# kvtml contained in tar files will be extracted to get the data from it. it can be deleted later.
import tarfile as tar

#from textnormalize import text_normalize_filter

#directory stuff
from dircache import listdir
from os.path import isdir

#for the time - this still has to be improved or done in a different way - file time?
import time

#unicode
import codecs


html_file_name=''
meta_file_name='kvtml2.meta'
provider_file=None	

class KVTML_2_Handler(ContentHandler):
	KVTML_VERSION = 0

	#important tags
	CAPTURE_TITLE = 1
	CAPTURE_AUTHOR = 2
	CAPTURE_EMAIL = 3
	CAPTURE_LICENCE = 4
	CAPTURE_COMMENT = 5
	CAPTURE_GENERATOR = 6
	CAPTURE_LANGUAGES = 7
	CAPTURE_LANGUAGE_NAME = 8
	CAPTURE_LANGUAGE_LOCALE = 9
	CAPTURE_DATE = 10
	COUNT_LESSONS = 11
	

	def __init__(self):
		self.inInformation=False
		self.title=""
		self.license=""
		self.author=""
		self.email=""
		self.comment=""
		self.changedate=""
		self.generator=""
		self._language_name=""
		self._language_locale=""
		self.languages={}
		self.lessonCounter=0
		self.entryCounter=0
		#Track the item being constructed in the current dictionary
		self._item_to_create = None
		self._state = None
		return


	def startDocument(self):
		pass

	def startElement(self, name, attrs):
		if (name == u"information"):
			self.inInformation=True
		
		if (self.inInformation):
			if (name == u"title"):
				self._state = self.CAPTURE_TITLE
			if (name == u"author"):
				self._state = self.CAPTURE_AUTHOR
			if (name == u"email"):
				self._state = self.CAPTURE_EMAIL
			if (name == u"license"):
				self._state = self.CAPTURE_LICENCE
			if (name == u"comment"):
				self._state = self.CAPTURE_COMMENT
			if (name == u"generator"):
				self._state = self.CAPTURE_GENERATOR
			if (name == u"date"):
				self._state = self.CAPTURE_DATE

		if (name == u"identifiers"):
			self._state = self.CAPTURE_LANGUAGES

		if (name == u"entry"):
			self.entryCounter += 1
		
		if (name == u"lessons"):
			self._state = self.COUNT_LESSONS
		
		if (self._state==self.COUNT_LESSONS):
			if (name == u"container"):
				self.lessonCounter += 1

		if (self._state==self.CAPTURE_LANGUAGES):
			if(name == u"name"):
				self._state = self.CAPTURE_LANGUAGE_NAME
			if(name == u"locale"):
				self._state = self.CAPTURE_LANGUAGE_LOCALE
			

	def endElement(self, name):
		if (name == u"information"):
			self.inInformation=False
		if (name == u"identifier"):
			self.languages[self._language_locale] = self._language_name
		if (name == u"lessons"):
			self._state = None

		if self._state in [self.CAPTURE_TITLE, self.CAPTURE_AUTHOR, self.CAPTURE_LICENCE,
					self.CAPTURE_COMMENT, self.CAPTURE_GENERATOR, self.CAPTURE_EMAIL, self.CAPTURE_DATE]:
			self._state = None
 
	def characters(self, text):
		if self._state == self.CAPTURE_TITLE:
			self.title = text
		if self._state == self.CAPTURE_AUTHOR:
			self.author = text
		if self._state == self.CAPTURE_EMAIL:
			self.email = text
		if self._state == self.CAPTURE_LICENCE:
			self.license = text
		if self._state == self.CAPTURE_COMMENT:
			self.comment = self.comment + text

		if self._state == self.CAPTURE_DATE:
			self.changedate = text
		if self._state == self.CAPTURE_GENERATOR:
			self.generator = text
		
		if self._state == self.CAPTURE_LANGUAGE_NAME:
			self._language_name = text
		if self._state == self.CAPTURE_LANGUAGE_LOCALE:
			self._language_locale = text

		if self._state in [self.CAPTURE_LANGUAGES, self.CAPTURE_LANGUAGE_NAME, self.CAPTURE_LANGUAGE_LOCALE]:
			self._state = self.CAPTURE_LANGUAGES

	def endDocument(self):
		#list of languages
		self.langs_xml=""
		self.langs_htmlSeperated=""

		for lang in self.languages.values():
			self.langs_xml += lang + " - "
			self.langs_htmlSeperated += lang + "<br/>"
				
		#replace last "-" with ;
		self.summary = self.langs_xml[:-3] + "; "
		self.numbers = str(self.lessonCounter) + " Lessons, " + str(self.entryCounter) + " Entries\n"
		self.summary += self.numbers
		self.summary += self.comment

		self.summary_html = "<b>" + self.title + "</b><br/>"
		self.summary_html += self.comment + "<br/>"
		self.summary_html += self.numbers

		#dictionary with most tags:
		self._params = {'category': saxutils.escape('application/x-kvtml'), 
			'title': saxutils.escape(self.title), 
			'author': saxutils.escape(self.author), 
			'email': saxutils.escape(self.email), 
			'changedate': saxutils.escape(self.changedate), 
			'license': saxutils.escape(self.license), 
			'summary': saxutils.escape(self.summary),
			#do not escape, we want html here!
			'summary_html': self.summary_html,
			'version': saxutils.escape('1.0'), 
			'languages_xml': self.langs_xml,
			#do not escape, we want html here!
			'languages_htmlSeperated': self.langs_htmlSeperated
			}

	def xmlData(self, payloadfile):
		return self._xmlMetaData('some date', payloadfile)


	def _xmlMetaData(self, releasedate, payloadfile):
		self._params['releasedate'] = saxutils.escape(releasedate)
		self._params['payloadfile'] = saxutils.escape(payloadfile)

		datastring = \
u"""<stuff category="%(category)s">
	<name>%(title)s</name>
	<author>%(author)s</author>
	<email>%(email)s</email>
	<license>%(license)s</license>
	<summary>%(summary)s</summary>
	<version>%(version)s</version>
	<releasedate>%(releasedate)s</releasedate>
        <payload>http://files.kde.org/edu/kvtml2/%(payloadfile)s</payload>
</stuff>
""" % self._params
		return datastring



	def tableHtml(self, filename):
		self._params['filename'] = saxutils.escape(filename)
		datastring = \
u"""<tr>
   <td align="center"><a href="%(filename)s">%(languages_htmlSeperated)s</a></td>
   <td>%(summary_html)s</td>
   <td>%(author)s</td>
   <td>%(changedate)s</td>
   <td align="center"><a href="%(filename)s"><img src="../../pics/projects/ox32-app-parley.png" alt="download"></a></td>
</tr>
""" % self._params
#maybe some php voodoo for the date? <?php echo date ("Y-m-d", filemtime(%(filename)s)); ?>
		return datastring




def processFile(target_file, kvtml_file):
	#kvtml = parse(open(voc_file))
	#root = kvtml.getElementsByTagName("kvtml")[0]
	#readLanguages(root)

	xmlhandler_kvtml2 = KVTML_2_Handler()
	saxparser = make_parser()
	saxparser.setContentHandler(xmlhandler_kvtml2)

	datasource = open(kvtml_file,"r")
	saxparser.parse(datasource)

	ghnsMeta = xmlhandler_kvtml2.xmlData(target_file)
	tableHtml = xmlhandler_kvtml2.tableHtml(target_file)

	metaFile = codecs.open(target_file + ".meta", "w", "utf-8")
	metaFile.write(ghnsMeta)
	metaFile.close()

	provider_file.write(ghnsMeta)

	htmlFile = codecs.open(html_file_name, 'a', "utf-8")
	htmlFile.write(tableHtml)
	htmlFile.close()


def readFile(path):
	print "Reading " + path
	processFile(path, path)

def readCompressedFile(path):
	print "Reading " + path
	archive = tar.open(path)
	for name in archive.getnames():
		if name.endswith(".kvtml"):
			print "extracting: ", name
			#create a tmp dir in the dir of the tar
			archive.extract(name, "tmp")

			index = path.rfind("/")
			tempfile = "tmp/" + name

			processFile(path, tempfile)	
	
	archive.close()


def initProvider():
	global provider_file
	provider_file = codecs.open("provider41.xml", 'w', "utf-8")
	provider_file.write("""<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE knewstuff SYSTEM "knewstuff.dtd">
<knewstuff>
""")

def finalizeProvider():
	provider_file.write("</knewstuff>")
	provider_file.close()


def readDirectory(path):
	"""iteratively parse directories for kvtml and tar-bzip2"""
	for entry in listdir(path):
		if isdir(path + "/" + entry):
			#print path + "/" + entry
			if entry != "tmp":
				readDirectory(path + "/" + entry)
		else:
			#print "a file: " + path + "/" + entry
			global html_file_name

			html_file_name="table_" + path.lstrip("./").replace("/", "_") + ".inc"
			if entry.endswith(".kvtml"):
				readFile(path + "/" + entry)

			elif (entry.endswith(".tbz2") or entry.endswith(".tar.bz2")):
				readCompressedFile(path + "/" + entry)

def main():
			
	print "Hello, this is your kvtml file meta data updater speaking..."
	initProvider()
	readDirectory(".")
	finalizeProvider()
main()
