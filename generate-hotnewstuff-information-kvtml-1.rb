#!/usr/bin/env ruby
#
# Ruby script for generating a KGetHotNewStuff file
#
# Copyright 2007 Carsten Niehaus <cniehaus@gmx.de>
# the original script for Kalzium
# Copyright 2007 Frederik Gladhorn <frederik.gladhorn@kdemail.net>
# modification to work with kvtml-1 files
# License: GPL V2

require "rexml/document"


#return the XML for the given KVTML file

def xmlFromKvtml(filename, directory) 
	title = ""
	author = ""
	license = ""
	generator = ""
	file = File.new( filename )
	doc = REXML::Document.new file
	doc.elements.each("kvtml") { |element| 
		title = element.attributes["title"]
		author = element.attributes["author"]
		license = element.attributes["license"]
		generator = element.attributes["generator"]
	}

	aString = <<EOS
	<stuff category="application/x-kvtml">
                <name>#{title}</name>
                <author>#{author}</author>
                <email></email>
                <license>#{license}</license>
                <summary>Vocabulary file</summary>
                <version></version>
                <releasedate></releasedate>
                <payload>http://files.kde.org/edu/kvtml/#{filename}</payload>
        </stuff>
EOS
end

#maybe we'll have subdirectories later
directories = ["kvtml"]

#a global counter for the files
@@counter = 1

#go over all directories
pwd = Dir.getwd
directories.each{ |d|
	Dir.chdir( "#{pwd}/#{d}" )
	list = Dir.glob("*.kvtml")

	xmloutputfile = File.new( "hotnewstuff.xml", File::CREAT|File::TRUNC|File::RDWR )

	xmloutputfile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
	xmloutputfile << "<!DOCTYPE knewstuff SYSTEM \"knewstuff.dtd\">\n"
	xmloutputfile << "<knewstuff>\n"

	#in each directory parse all files, get the valid XML and put the XML in the file
	list.each{|filename| 
		puts "#{@@counter}: " +  "Parsing #{filename}".center( 100, "-" )
		xmloutputfile <<  xmlFromKvtml(filename,d)
		@@counter+=1
	}
	xmloutputfile << "</knewstuff>"
}

